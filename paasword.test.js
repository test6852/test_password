const {checkLength,checkAlphabet,checkDigit,checkSymbol,checkPassword} =  require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to ture ', ()=> {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to ture ', ()=> {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to ture ', ()=> {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 25 characters to false ', ()=> {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Password Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has not alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has not alphabet 1111 in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Password Digit', () => {
  test('should has Digit 9 in password', () => {
    expect(checkDigit('9')).toBe(true)
  })

  test('should has Digit 11 in password', () => {
    expect(checkDigit('11')).toBe(true)
  })

  test('should has Digit mon in password', () => {
    expect(checkDigit('mon')).toBe(false)
  })
})


describe('Test Password Symbol', () => {
  test('should has Symbol @ in password', () => {
    expect(checkSymbol('12@34')).toBe(true)
  })

  test('should has Symbol / in password', () => {
    expect(checkSymbol('00/70')).toBe(true)
  })

  test('should has not Symbol w070 in password', () => {
    expect(checkSymbol('w070')).toBe(false)
  })
})

describe('Test Password ', () => {
  test('should has Password to be flase', () => {
    expect(checkPassword('Mon@98')).toBe(false)
  })

  test('should has Password to be true', () => {
    expect(checkPassword('Wimon@987')).toBe(true)
  })
})