const checkLength = function (password) {
  return password.length >= 8 && password.length <= 25 
}

const checkAlphabet = function (password) {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz'
  for (const ch of password) {
    if (alphabet.includes(ch.toLowerCase())) return true
  }
  return false
}


const checkDigit = function (password) {
  const Digit = '0123456789'
  for (const ch of password) {
    if (Digit.includes(ch.toLowerCase())) return true
  }
  return false
}


const checkSymbol = function (password) {
  const Symbol = '(!"#$%&()*+,-./:;<=>?@[]^_`{|}~)'
  for (const ch of password) {
    if (Symbol.includes(ch.toLowerCase())) return true
  }
  return false
}


const checkPassword = function (password) {
  return checkLength(password) &&
  checkAlphabet(password) &&
  checkDigit(password) &&
  checkSymbol(password) 

}


module.exports = {
  checkLength,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkPassword
}
